# tests/icm20948_test.py
#
# Run tests with 'python -m pytest' in main directory
#
# Copyright (c) 2022 Spring City Solutions LLC and other contributors
# Licensed under the Apache License, Version 2.0

import pytest

import icm20948

def test_twos_complement_to_integer_16bit():
    assert icm20948.twos_complement_to_integer_16bit(0x7FFF) == 32767
    assert icm20948.twos_complement_to_integer_16bit(0x0010) == 16
    assert icm20948.twos_complement_to_integer_16bit(0x0001) == 1
    assert icm20948.twos_complement_to_integer_16bit(0x0000) == 0
    assert icm20948.twos_complement_to_integer_16bit(0xFFFF) == -1
    assert icm20948.twos_complement_to_integer_16bit(0xFFF0) == -16
    assert icm20948.twos_complement_to_integer_16bit(0x8001) == -32767
    assert icm20948.twos_complement_to_integer_16bit(0x8000) == -32768
