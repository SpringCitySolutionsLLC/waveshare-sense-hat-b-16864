# tests/lps22hb_test.py
#
# Run tests with 'python -m pytest' in main directory
#
# Copyright (c) 2022 Spring City Solutions LLC and other contributors
# Licensed under the Apache License, Version 2.0

import pytest

import lps22hb

def test_parse_args_default():
    parser = lps22hb.parse_args([])
    assert lps22hb.register_data['ctrl_reg'] == 0x000100

def test_parse_args_lpf():
    parser = lps22hb.parse_args(['--lpf', 'off'])
    assert lps22hb.register_data['ctrl_reg'] == 0x000100
    parser = lps22hb.parse_args(['--lpf', '10'])
    assert lps22hb.register_data['ctrl_reg'] == 0x000108
    parser = lps22hb.parse_args(['--lpf', '20'])
    assert lps22hb.register_data['ctrl_reg'] == 0x00010C

def test_twos_complement_to_integer_16bit():
    assert lps22hb.twos_complement_to_integer_16bit(0x7FFF) == 32767
    assert lps22hb.twos_complement_to_integer_16bit(0x0010) == 16
    assert lps22hb.twos_complement_to_integer_16bit(0x0001) == 1
    assert lps22hb.twos_complement_to_integer_16bit(0x0000) == 0
    assert lps22hb.twos_complement_to_integer_16bit(0xFFFF) == -1
    assert lps22hb.twos_complement_to_integer_16bit(0xFFF0) == -16
    assert lps22hb.twos_complement_to_integer_16bit(0x8001) == -32767
    assert lps22hb.twos_complement_to_integer_16bit(0x8000) == -32768

def test_integer_16bit_to_twos_complement():
    assert lps22hb.integer_16bit_to_twos_complement("insane user input") == 0x0000
    assert lps22hb.integer_16bit_to_twos_complement(32767) == 0x7FFF
    assert lps22hb.integer_16bit_to_twos_complement(16) == 0x0010
    assert lps22hb.integer_16bit_to_twos_complement(1) == 0x0001
    assert lps22hb.integer_16bit_to_twos_complement(0) == 0x0000
    assert lps22hb.integer_16bit_to_twos_complement(-1) == 0xFFFF
    assert lps22hb.integer_16bit_to_twos_complement(-16) == 0xFFF0
    assert lps22hb.integer_16bit_to_twos_complement(-32767) == 0x8001
    assert lps22hb.integer_16bit_to_twos_complement(-32768) == 0x8000

def test_twos_complement_to_integer_24bit():
    assert lps22hb.twos_complement_to_integer_24bit(0x7FFFFF) == 8388607
    assert lps22hb.twos_complement_to_integer_24bit(0x000010) == 16
    assert lps22hb.twos_complement_to_integer_24bit(0x000001) == 1
    assert lps22hb.twos_complement_to_integer_24bit(0x000000) == 0
    assert lps22hb.twos_complement_to_integer_24bit(0xFFFFFF) == -1
    assert lps22hb.twos_complement_to_integer_24bit(0xFFFFF0) == -16
    assert lps22hb.twos_complement_to_integer_24bit(0x800001) == -8388607
    assert lps22hb.twos_complement_to_integer_24bit(0x800000) == -8388608
