# tests/ads1015_test.py
#
# Run tests with 'python -m pytest' in main directory
#
# Copyright (c) 2022 Spring City Solutions LLC and other contributors
# Licensed under the Apache License, Version 2.0

import pytest

import ads1015

def test_parse_args_default():
    parser = ads1015.parse_args([])
    assert ads1015.register_data['config'] == 0xC503

def test_parse_args_dr():
    parser = ads1015.parse_args(['--dr', '128'])
    assert ads1015.register_data['config'] == 0xC503
    parser = ads1015.parse_args(['--dr', '250'])
    assert ads1015.register_data['config'] == 0xC523
    parser = ads1015.parse_args(['--dr', '490'])
    assert ads1015.register_data['config'] == 0xC543
    parser = ads1015.parse_args(['--dr', '920'])
    assert ads1015.register_data['config'] == 0xC563
    parser = ads1015.parse_args(['--dr', '1600'])
    assert ads1015.register_data['config'] == 0xC583
    parser = ads1015.parse_args(['--dr', '2400'])
    assert ads1015.register_data['config'] == 0xC5A3
    parser = ads1015.parse_args(['--dr', '3300'])
    assert ads1015.register_data['config'] == 0xC5E3

def test_parse_args_fsr():
    parser = ads1015.parse_args(['--fsr', '6.144'])
    assert ads1015.register_data['config'] == 0xC103
    parser = ads1015.parse_args(['--fsr', '4.096'])
    assert ads1015.register_data['config'] == 0xC303
    parser = ads1015.parse_args(['--fsr', '2.048'])
    assert ads1015.register_data['config'] == 0xC503
    parser = ads1015.parse_args(['--fsr', '1.024'])
    assert ads1015.register_data['config'] == 0xC703
    parser = ads1015.parse_args(['--fsr', '0.512'])
    assert ads1015.register_data['config'] == 0xC903
    parser = ads1015.parse_args(['--fsr', '0.256'])
    assert ads1015.register_data['config'] == 0xCB03

def test_parse_args_input():
    parser = ads1015.parse_args(['--input', '0G'])
    assert ads1015.register_data['config'] == 0xC503
    parser = ads1015.parse_args(['--input', '1G'])
    assert ads1015.register_data['config'] == 0xD503
    parser = ads1015.parse_args(['--input', '2G'])
    assert ads1015.register_data['config'] == 0xE503
    parser = ads1015.parse_args(['--input', '3G'])
    assert ads1015.register_data['config'] == 0xF503
    parser = ads1015.parse_args(['--input', '01'])
    assert ads1015.register_data['config'] == 0x8503
    parser = ads1015.parse_args(['--input', '03'])
    assert ads1015.register_data['config'] == 0x9503
    parser = ads1015.parse_args(['--input', '13'])
    assert ads1015.register_data['config'] == 0xA503
    parser = ads1015.parse_args(['--input', '23'])
    assert ads1015.register_data['config'] == 0xB503    

# A good explanation of converting 12 bit twos complement 
# to signed integer is found in the datasheet on page 22
def test_twos_complement_to_integer_12bit():
    assert ads1015.twos_complement_to_integer_12bit(0x7FF) == 2047
    assert ads1015.twos_complement_to_integer_12bit(0x010) == 16
    assert ads1015.twos_complement_to_integer_12bit(0x001) == 1
    assert ads1015.twos_complement_to_integer_12bit(0x000) == 0
    assert ads1015.twos_complement_to_integer_12bit(0xFFF) == -1
    assert ads1015.twos_complement_to_integer_12bit(0xFF0) == -16
    assert ads1015.twos_complement_to_integer_12bit(0x801) == -2047
    assert ads1015.twos_complement_to_integer_12bit(0x800) == -2048
