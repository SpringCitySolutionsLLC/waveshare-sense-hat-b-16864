# tests/tcs34725_test.py
#
# Run tests with 'python -m pytest' in main directory
#
# Copyright (c) 2022 Spring City Solutions LLC and other contributors
# Licensed under the Apache License, Version 2.0

import pytest

import tcs34725

def test_parse_args_default():
    parser = tcs34725.parse_args([])
    assert tcs34725.register_data['control'] == 0x02

def test_parse_args_gain():
    parser = tcs34725.parse_args(['--gain', '1'])
    assert tcs34725.register_data['control'] == 0x00
    parser = tcs34725.parse_args(['--gain', '4'])
    assert tcs34725.register_data['control'] == 0x01
    parser = tcs34725.parse_args(['--gain', '16'])
    assert tcs34725.register_data['control'] == 0x02
    parser = tcs34725.parse_args(['--gain', '60'])
    assert tcs34725.register_data['control'] == 0x03
    
# Test examples are literally taken from page 23 of the datasheet
def test_parse_args_time():
    parser = tcs34725.parse_args(['--time', '2.4'])
    assert tcs34725.register_data['rgbc'] == 0xFF
    parser = tcs34725.parse_args(['--time', '24'])
    assert tcs34725.register_data['rgbc'] == 0xF6
    parser = tcs34725.parse_args(['--time', '101'])
    assert tcs34725.register_data['rgbc'] == 0xD5
    # Rounding errors, I return 0xBF instead of 0xC0
    # parser = tcs34725.parse_args(['--time', '154'])
    # assert tcs34725.register_data['rgbc'] == 0xC0
    parser = tcs34725.parse_args(['--time', '614'])
    assert tcs34725.register_data['rgbc'] == 0x00
