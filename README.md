node-red-contrib-waveshare-sense-hat-b-16864
=====================

A set of <a href="http://nodered.org">Node-RED</a> nodes to control the numerous 
hardware components on the Waveshare(tm) Sense Hat (B) model aka 16864 which is
a Raspberry Pi expansion board.

The sensors on this board all use I2C, remember to enable I2C when 
running "sudo raspi-config"

Simply plug in the shield, install this Node-RED node, the hardware just works
with no configuration required.

This board contains the following sensors:
ICM20948 accelerometer / gyroscope / magnetometer
SHTC3 temperature / humidity
LPS22HB barometer
TCS34725 color sensor
ADS1015 four channel twelve bit ADC

See the <a href="https://gitlab.com/SpringCitySolutionsLLC/waveshare-sense-hat-b-16864/-/wikis/home">wiki</a> for more details.

## Install

You can watch the Youtube video describing how to set up the board:
(To be Added)

Either use the Node-RED Menu - Manage Palette option to install, or run the following
command in your Node-RED user directory - typically `~/.node-red`

        npm i node-red-contrib-waveshare-sense-hat-b-16864

Enable I2C using raspi-config

Run the following line at the Pi command line:
sudo apt-get install i2c-tools python3-smbus

Run i2cdetect 1 and hit y and you should see the devices on 29, 48, 5c, 68, and 70
If this works, the nodes will work.
If this does not work, then you probably need to enable I2C or maybe reboot the Pi.

## Usage

Here is a link to the Youtube video describing how to use the software:
(To be Added)

Also you might want to watch the Youtube video showing electronics test 
bench performance of the hardware:
(To be Added)

The analog input, barometer, light sensor, temperature, humidity, magnetometer, gyroscope, and accelerometer all work.

#### Input

 - `msg.payload` - *boolean | number | string*

Any `msg.payload` will initiate a sample of any of the devices.

## Funding

Please consider becoming a patron of Spring City Solutions LLC
Your support directly funds the development of new drivers 
and nodes for Node-RED.

For the latest news, detailed daily dev logs are posted on 
the Patreon page.

https://www.patreon.com/springcitysolutions_nodered

## Links

Hardware Links:

Board Manufacturer Store:
https://www.waveshare.com/sense-hat-b.htm

Board Manufacturer Wiki:
https://www.waveshare.com/wiki/Sense_HAT_(B)

ADS1015 four channel twelve bit ADC from Texas Instruments (tm):
https://www.ti.com/product/ADS1015

Free Ebook "Fundamentals of Precision ADC Noise Analysis":
https://www.ti.com/lit/pdf/slyy192

Analog Design Journal Article "How delta-sigma ADCs work" two parts:
https://www.ti.com/lit/pdf/slyt423
https://www.ti.com/lit/pdf/slyt438

Application Note "SBAA230" Digital Filter Types in Delta Sigma ADCs:
https://www.ti.com/lit/pdf/sbaa230

Grove Protoshield (Not a part of the board but IS a part of my development system):
http://wiki.seeedstudio.com/Grove-Protoshield/

ICM-20948 accelerometer / gyroscope / magnetometer from TDK (tm):
https://invensense.tdk.com/products/motion-tracking/9-axis/icm-20948/

LPS22HB barometer from ST Microelectronics (tm) :
https://www.st.com/en/mems-and-sensors/lps22hb.html

Application Note AN4833 "Measuring pressure data from ST's LPS22HB digital pressure sensor" :
https://www.st.com/resource/en/application_note/an4833-measuring-pressure-data-from-sts-lps22hb-digital-pressure-sensor-stmicroelectronics.pdf

Technical Note TN1229 "How to interpret pressure and temperature readings in the LPS22HB pressure sensor" :
https://www.st.com/resource/en/technical_note/tn1229-how-to-interpret-pressure-and-temperature-readings-in-the-lps22hb-pressure-sensor-stmicroelectronics.pdf

RT9193 voltage regulator from RICHTEK (tm):
https://www.richtek.com/assets/product_file/RT9193/DS9193-16.pdf

SHTC3 temperature / humidity from Sensirion (tm):
https://www.sensirion.com/en/environmental-sensors/humidity-sensors/digital-humidity-sensor-shtc3-our-new-standard-for-consumer-electronics/

TCS34725 color sensor from AMS (tm) :
https://ams.com/tcs34725

Application Note DN20 Colorimetry Tutorial :
https://ams.com/documents/20143/36005/LightSensors_AN000519_1-00.pdf/d2a0670d-7557-ed94-cc0e-225cfa8cb031

Application Note DN25 Calculating Color Temperature and Illuminance :
https://ams.com/documents/20143/36005/TCS34xx_AN000517_1-00.pdf/616f6ed0-8409-a21d-cd85-307a1e377102

Application Note Improving Color Sensor Lux Accuracy :
https://ams.com/documents/20143/36005/ColorSensors_AN000261_1-00.pdf/5d76b23f-7670-505c-a4d2-94cae758855e

Application Note DN40 Lux and CCT calculations :
https://ams.com/documents/20143/36005/ColorSensors_AN000166_1-00.pdf/d1290c78-4ef1-5b88-bff0-8e80c2f92b6b

Spring City Solutions Links:

Spring City Solutions Node-RED project page:
https://www.springcitysolutions.com/nodered

Spring City Solutions page for this node:
https://www.springcitysolutions.com/nodered-waveshare-sense-hat

Spring City Solutions Node-RED project email:
nodered@springcitysolutions.com

Spring City Solutions Gitlab for this node:
https://gitlab.com/SpringCitySolutionsLLC/waveshare-sense-hat-b-16864

Patreon Page:
https://www.patreon.com/springcitysolutions_nodered

Software Links:

Spring City Solutions Gitlab for this node:
https://gitlab.com/SpringCitySolutionsLLC/waveshare-sense-hat-b-16864

Doxygen docs for this node autogenerated by Gitlab CI/CD:
https://springcitysolutionsllc.gitlab.io/waveshare-sense-hat-b-16864/index.html

npmjs for this node:
https://www.npmjs.com/package/node-red-contrib-waveshare-sense-hat-b-16864

Node-RED flows for this node:
https://flows.nodered.org/node/node-red-contrib-waveshare-sense-hat-b-16864

Documentation Links:

Gitlab wiki for this node (the master copy of list of links is here):
https://gitlab.com/SpringCitySolutionsLLC/waveshare-sense-hat-b-16864/-/wikis/home

Youtube video "How to set up":
TODO

Youtube video "How to use":
TODO

Youtube video "Testing Results":
TODO

## Trademarks

AMS (tm) is a trademark of AMS Osram Group AG

Node-RED and node.js are a trademark of the OpenJS Foundation in the United States

Python is a registered trademark of the Python Software Foundation.

Raspberry Pi is a trademark of Raspberry Pi Trading

Richtek is a registered trademark of Richtek Technology Corporation

Sensirion (tm) is a trademark of Sensirion AG

ST Microelectronics (tm) is a trademark of STMICROELECTRONICS, INC.

TDK (tm) is a trademark of TDK KABUSHIKI KAISHA TDK CORPORATION

Texas Instruments (tm) is a trademark of Texas Instruments

WAVESHARE (tm) is a trademark of Shenzhen Weixue Electronic Co., Ltd.

"All trademarks are property of their respective owners"

## Copyrights and Licenses

Majority of code Copyright (c) 2021-2022 Spring City Solutions LLC and other
contributors and licensed under the Apache License, Version 2.0

Icon made from http://www.onlinewebfonts.com/icon aka Icon Fonts is licensed by CC BY 3.0